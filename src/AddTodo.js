import React from 'react';

const AddTodo = ({handleInputChange, handleTodoSubmit, value}) => {
    return (
        <div>
            <form className="row" onSubmit={(e) => handleTodoSubmit(e)}>
                <div className="col padding-right-small">
                    <input type="text" placeholder="New Todo" onChange={(e) => handleInputChange(e)} value={value}/>
                </div>
                <div className="col padding-left-small">
                    <input type="submit" className="papper-btn btn-small"/>
                </div>
            </form>
        </div>
    );
}

export default AddTodo;