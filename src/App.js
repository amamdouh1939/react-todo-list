import React, { Component } from 'react';
import styled from 'styled-components';
import AddTodo from './AddTodo';
import TodoList from './TodoList';
import Todo from './Todo';

const Container = styled.div`
    background-color: #f1f1f1;
    margin: 0;
    min-height: 100vh;
`;

const Paper = styled.div`
    width: 480px;
    background-color: white;
`;

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            currentId: 0,
            input: '',
            todos: []
        }
    }


    onInputChange = (e) => {
        let input = e.target.value;
        this.setState({input});
        console.log(this.state);
    }

    onTodoSubmit = (e) => {
        e.preventDefault();
        let currentId = this.state.currentId + 1;

        const newTodo = {
            id: currentId,
            text: this.state.input,
            completed: false
        }
        this.setState({
            input: '',
            currentId: currentId,
            todos: [...this.state.todos, newTodo]
        });
    }

    onClearTodoList = () => {
        this.setState({
            input: '',
            todos: [],
            currentId: 0
        })
    }
    
    switchState = (id) => {
        let todos = this.state.todos.filter(todo => {
            if (todo.id === id) {
                todo.completed = ! todo.completed;
            }
            return true;
        })

        this.setState({todos});
    }


    render() {
        return (
            <Container className="row flex-center flex-middle">
                <Paper className="border border-primary padding-large margin-large no-responsive">
                    <h2 className="text-center">TODO-LIST</h2>
                    <AddTodo handleInputChange={this.onInputChange} handleTodoSubmit={this.onTodoSubmit} value={this.state.input}/>
                    <TodoList todos={this.state.todos} switchState={this.switchState}/>
                    <button type="button" className="paper-btn btn-small" onClick={() => this.onClearTodoList()}>Clear</button>
                </Paper>
            </Container>
        )
    }

}


export default App;
