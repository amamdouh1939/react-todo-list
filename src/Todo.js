import React from 'react';
import styled from 'styled-components';

const Item = styled.li`
    padding-left: 1em;
    cursor: pointer;
    overflow-wrap: break-word;
    &:before {
        content: ""
    }
`;

const Todo = ({text, id, completed, switchState}) => {
    return (
        <Item className={"padding-small margin-small " + (completed ? 'background-primary' : 'shadow shadow-hover')} onClick={() => switchState(id)} style={completed ? {'text-decoration': 'line-through'} : {}}>
            {text}
        </Item>
    )
}

export default Todo;