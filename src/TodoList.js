import React from 'react';
import styled from 'styled-components';
import Todo from './Todo';

const List = styled.ul`
    padding-left: 5px;
`;

const TodoList = ({todos, switchState}) => {
    return (
        <List className="child-borders">
            {todos.map(todo => <Todo text={todo.text} id={todo.id} key={todo.id} completed={todo.completed} switchState={switchState}/>)}
        </List>
    )
}

export default TodoList;