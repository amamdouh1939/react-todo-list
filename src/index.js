import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'papercss/dist/paper.min.css'

ReactDOM.render(<App />, document.getElementById('root'));